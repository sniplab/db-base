# README for copy_from:

RUN:
```
1. activate fish env variables (cd .)
2. from current directory: python copy_from.py
```

TO KNOW:
1. db table must be pre-created for file

2. if you need ID, it must be in the file (for copy_from insert method)
This way is good, if you need to insert RAW data from file

3. Usually It is better to use \t as separator
because comma can appear somewhere in the text
as well as tab, but tab is less common