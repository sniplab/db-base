import os
import psycopg2


def create_table_sql(table_name) -> str:
    # ID!!!!!
    # return """
    #     CREATE TABLE IF NOT EXISTS {0} (
    #     "id" integer PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY,
    #     "user_name" text,
    #     "gender" text
    #     );
    # """.format(table_name)
    return """
        CREATE TABLE IF NOT EXISTS {0} (
        "user_name" text,
        "gender" text
        );
    """.format(table_name)


credentials = dict(
    database=os.environ["DB_NAME"],
    user=os.environ["DB_USER"],
    password=os.environ["DB_PASS"],
    host=os.environ["DB_HOST"],
    port=int(os.environ["DB_PORT"]),
    sslmode=os.environ["DB_SSL"],
)

connection = psycopg2.connect(**credentials)

file = open('my.csv', 'r')
with connection.cursor() as cur:
    cur.execute(create_table_sql(table_name="users"))
    cur.copy_from(file, "users", sep=',')
    connection.commit()

